<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("Barang_models");
		$this->load->model("Jenisbarang_models");
	}

	public function index()
	{
		$this->data_barang();
	}
	public function data_barang()
	{
		$data['data_barang'] = $this->Barang_models->tampilDataBarang();
		$this->load->view('data_barang', $data);
	}
	public function detailbarang($kode_barang)
	{
		$data['data_barang'] =$this->Barang_models->detail($kode_barang);
		$this->load->view('detailbarang', $data);
	}

	public function inputbarang()
	{
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();

		if (!empty($_REQUEST)){
			$m_barang = $this->Barang_models;
			$m_barang->save();
			redirect("Barang/index", "refresh");
		}


		$this->load->view('inputbarang',$data);
	}
	
	public function editbarang($kode_barang)
	{	
		$data['data_jenisbarang'] = $this->Jenisbarang_models->tampilDataJenisbarang();
		$data['detail_barang']	= $this->Barang_models->detail($kode_barang);

		if (!empty($_REQUEST)){
			$m_barang = $this->Barang_models;
			$m_barang->update($kode_barang);
			redirect("Barang/index", "refresh");
		}
		$this->load->view('editbarang', $data);	
	}
	public function delete($kode_barang)
	{
		$m_barang = $this->Barang_models;
		$m_barang->delete($kode_barang);	
		redirect("Barang/index", "refresh");	
	}
	

}
